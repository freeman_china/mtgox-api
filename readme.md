# Unofficial Documentation for MtGox's HTTP API v2

---

Tips and donations: `1VvKzcFibZdMyQdbXpSYcyoYvBsLLodDa`

Feedback here: <https://bitcointalk.org/index.php?topic=164404.0>

**If you just want information on each method, go [here](#markdown-header-contents)**

### Sections

1. [Updates](#markdown-header-updates)
2. [Background](#markdown-header-background)
3. [General Information](#markdown-header-general-information)
	* [MtGox](#markdown-header-mtgox)
	* [Currencies](#markdown-header-currencies)
	* [Security](#markdown-header-security)
		* [Tonces](#markdown-header-tonces)
		* [HMAC Troubleshooting](#markdown-header-hmac-troubleshooting)
			* [Javascript](#markdown-header-javascript)
		* [Requests](#markdown-header-requests)
		* [WebSockets](#markdown-header-websockets)
	* [Requests](#markdown-header-requests)
	* [WebSockets](#markdown-header-websockets)
4. [Methods](#markdown-header-methods)
	* [Glossary](#markdown-header-glossary)
		* [Code](#markdown-header-code)
	* [Contents](#markdown-header-contents)
	* [Money](#markdown-header-money)
	* [Security](#markdown-header-security_1)
	* [Stream](#markdown-header-stream)
	* [Others](#markdown-header-other-methods)

### Updates

Click [here](#markdown-header-background) to skip!

##### 30th August, 2013:

* Added Link field to [`money/info`](#markdown-header-moneyinfo)
* Added [`money/bitcoin/get_address`](#markdown-header-moneybitcoinget_address)

##### 24th June, 2013:

Thanks to [imperative](https://bitbucket.org/imperative) for a clarification on [`money/order/result`](#markdown-header-moneyorderresult).

##### 31st May, 2013:

Reevaluated [`money/order/result`](#markdown-header-moneyorderresult). Previously, I had only tested it for open orders, which caused an error, and had assumed it just didn't work. Thanks to [mikhail_tomsk](https://bitcointalk.org/index.php?topic=220787.0) for the prompt and info.

##### 30th May, 2013:

Update on the trade data situation ([`money/trades/fetch`](#markdown-header-moneytradesfetch)). MagicalTux has uploaded the database (with data up to 23rd May) to Google's BigQuery service, and will soon (next couple weeks) begin to update it automatically (every 10mins-1hour). More information here - <https://bitcointalk.org/index.php?topic=218980.0>

##### 23rd May, 2013:

* **Important!!** I have been recommending ways in [`money/trades/fetch`](#markdown-header-moneytradesfetch) on how to obtain all trade data, but it seems that recent API changes have been introduced to discourage this. This is because too many people are downloading all the data over this method individually and they are taxing the servers too much, perhaps even contributing to the DDoS attacks. I have received an official response from MagicalTux however:

	```this api change was made because of the very high load caused by
	all the people downloading the trade history that way. we've been
	considering different solutions for this, and there's already a
	very wide variety of available dumps for any currency available
	on mtgox.```

	I cannot yet find any of these dumps, but he assured me that he would prepare something soon, and when this is done I will provide instructions on how to download these trade dumps. In the meantime, please abstain from this kind of activity - it is probably fine to use this method to maintain an already up-to-date dump, but please don't start downloading this data from scratch!

* If you need the data ASAP and can't wait for the official dumps, I have uploaded my basic dump data for USD and EUR up to 1369314732232756 / Thu, 23 May 2013 13:12:12 GMT. See [here](https://bitcointalk.org/index.php?topic=164404.msg2245629#msg2245629) for more information :)

* Added information on a new type of `nonce`, the [`tonce`](#markdown-header-tonces).

##### 21st May, 2013:

* The API seems to have become even stricter, and you may experience problems if you don't include a `Content-Type` of `application/x-www-form-urlencoded` - see [Security](#markdown-header-security).
* Building on the 11th May point - I would recommend that you use GET for any methods that don't return personalised information, and only use POST if GET doesn't work for a given method, as the API is liable to become even stricter in the future.

##### 11th May, 2013:

* Updated [`money/trades/fetch`](#markdown-header-moneytradesfetch) - MtGox has changed its API so that you can't access this method over POST anymore, nor can you retrieve all data seamlessly across the tid transition point.
* It is unclear whether other methods have been affected by this change.

##### 28th April, 2013:

Overhauled [`money/trades/fetch`](#markdown-header-moneytradesfetch), as the previous information was only accurate for trades after Jun 26, 2011. The new documentation should be accurate for all trades since the launch of MtGox.


##### 26th April, 2013:

The `money/bitcoin/send_simple` method cannot be used if two-factor authentication is enabled for withdrawals, though 2F auth doesn't seem to affect any other methods -- credit to [advanced](https://bitcointalk.org/index.php?topic=164404.msg1951755#msg1951755).

##### 23rd April, 2013:

* Added some brief information and pointers for the WebSocket API, and links to resources (see <https://bitbucket.org/nitrous/mtgox-api/src/master/socket>)
* Added [`stream/list_public`](#markdown-header-streamlist_public)

##### 21st April, 2013:

* More information added to [`money/wallet/history`](#markdown-header-moneywallethistory) - thanks [Baracuda](https://bitcointalk.org/index.php?action=profile;u=91268) :)
* Added JS specifics to the troubleshooting section

##### 20th April, 2013:

Added the [HMAC Troubleshooting](#markdown-header-hmac-troubleshooting) section, as several people have had trouble getting the API signature working properly.

##### 19th April, 2013:

[`money/wallet/history`](#markdown-header-moneywallethistory)

##### 17th April, 2013:

Documented the new [`money/ticker_fast`](#markdown-header-moneyticker_fast) method - thanks [advanced](https://bitcointalk.org/index.php?action=profile;u=20114) :).

##### 14th April, 2013:

The requirement of a valid API key and secret used to not be enforced for methods such as [`money/ticker`](#markdown-header-moneyticker), but now they are required for all methods.

Also, make sure your nonce is always increasing, if you have problems, make sure your nonce is at least unix time with millisecond resolution, and obtain a new API key to reset the nonce.

##### 9th April, 2013:

* [MtGox](#markdown-header-mtgox) - Added some miscellaneous notes on MtGox usage, regarding bitcoin transfers, minimum trade size and trade fees
* [`money/info`](#markdown-header-moneyinfo) - Forgot to include `"Trade_Fee"` in the json response - this is a decimal value giving your current percent trade fee. Also, `"Open_Orders"` appears to be deprecated.
* [`money/orders`](#markdown-header-moneyorders) - Order format is now known (it was slightly different from expected, so there has been a change from v1 to v2).
* [`money/order/add`](#markdown-header-moneyorderadd) - Request and response format now known.
* [`money/order/cancel`](#markdown-header-moneyordercancel) - Request and response format now known.
* [`money/order/result`](#markdown-header-moneyorderresult) - This method appears to be only partially implemented, and will return an error
* [`money/trades/cancelled`](#markdown-header-moneytradescancelled) - This method appears to be unimplemented.

All *documented* methods should now be fairly complete, however some response key purposes are unknown

## Background

I was looking to start building a trading bot using MtGox's API before I started investing in bitcoins, and even though version 1 of the API is available and well-documented, it seems that it will soon be redundant, just as version 0 is, with the advent of version 2. However, there is next to no documentation for version 2, so I have started this project to collect and compile an unofficial documentation for those who need it.

Not all the information may be 100% accurate or complete due to the limited information available, and detective work needed, but hopefully it will be suitably reliable for general use. In addition, I hope that this document will become increasingly accurate in the future as more official information and experience is gained. Currently, there is information on most of the actual trading methods, and there is detailed information on how to access the API. 

Upon completion, each method should give a brief description of its use, a table of arguments if applicable, one or two example requests, and brief but detailed bullet points of any useful information and peculiarities with regard to how the method is used in practice. I also plan to list which rights an API key needs to access each method.

Thank you, and good luck in developing your v2 programs!

## General Information

Each request is posted over HTTPS to MtGox's servers, with a generic URL format, e.g:

<https://data.mtgox.com/api/2/BTCUSD/money/ticker>

Each request path needs a currency pair, the first currency being `BTC`, and the second one of the currencies specified below in the currencies section. The currency pair is followed by an API method in URL path form, all of which are currently found in the `money` category.

Each request also needs to be signed using an API key and an HMAC hash constructed from an API secret and the post data.

### MtGox

The minimum trade size to buy OR sell is 0.01 BTC.

When directly sending bitcoins to MtGox, your bitcoins will not appear until at least 6 confirmations have occurred.

Each trade is accompanied by a MtGox fee, see <https://mtgox.com/fee-schedule> for full information, or below for a brief overview:

MtGox fees are based on your BTC trade volume over the last 30 days, the higher your volume the lower your fee, and the fee varies between 0.60% for most users to 0.25% for the biggest traders. The following fees have been in effect between December 2011 to at least April 2013:

Volume (up to) | Fee
--- | ---
100 | 0.60%
200 | 0.55%
500 | 0.53%
1,000 | 0.50%
2,000 | 0.46%
5,000 | 0.43%
10,000 | 0.40%
25,000 | 0.30%
50,000 | 0.29%
100,000 | 0.28%
250,000 | 0.27%
500,000 | 0.26%
inf | 0.25%


### Currencies

Symbol | Name | Divisions | SF
--- | --- | ----- | ---
BTC | Bitcoin | 100,000,000 | 1e8
USD | US Dollar | 100,000 | 1e5
GBP | Great British Pound | 100,000 | 1e5
EUR | Euro | 100,000 | 1e5
JPY | Japanese Yen | 1000 | 1e3
AUD | Australian Dollar | 100,000 | 1e5
CAD | Canadian Dollar | 100,000 | 1e5
CHF | Swiss Franc | 100,000 | 1e5
CNY | Chinese Yuan | 100,000 | 1e5
DKK | Danish Krone | 100,000 | 1e5
HKD | Hong Kong Dollar | 100,000 | 1e5
PLN | Polish Złoty | 100,000 | 1e5
RUB | Russian Rouble | 100,000 | 1e5
SEK | Swedish Krona | 1000 | 1e3
SGD | Singapore Dollar | 100,000 | 1e5
THB | Thai Baht | 100,000 | 1e5
NOK | Norwegian Krone | 100,000 | 1e5
CZK | Czech Koruna | 100,000 | 1e5

Note the divisions column (the SF column is the same, but gives each in standard form with the number of 0s). When communicating with the MtGox server, you will often have the opportunity to work with ints or floats. The float values are deprecated however, and ints are recommended as they are precise. The divisions tells you what units each of these values is in. So if, for example, you request a bitcoin value, and get a response of `{'value_int': 69655509977}`, then the amount in bitcoins is `69655509977/1e8`, or `696.55509977 BTC`. Of course there is no requirement that you perform these conversions, but you should be aware of the differences in value.

### Security

In order to use the API, you must obtain an API key and secret. To do this, login to <https://mtgox.com/security>, and open the `Advanced API Key Creation` section. You can give your API key a name, and specify which permissions (rights) it will have. Once you have customised it to your liking, click `Create Key`. Make sure to copy down your API secret, as the website will not show you it again after this point.

The API keys you have created are shown under `Current API Keys` after you refresh the page. From here, you can retrieve your API key if you forget it, and you can change what rights it has at any time. If you believe it has been compromised, you should **immediately revoke the api key by clicking on the red cross**

Your API key and secret will be used to authenticate with MtGox's servers. This is achieved via the use of two HTTP headers: `Rest-Key` and `Rest-Sign`

`Rest-Key` is your API key, `Rest-Sign` is an HMAC hash constructed from your API secret, your API method path, your post data, and uses the SHA-512 algorithm. The `Rest-Sign` is constructed as follows:

* Your API secret has been base-64 encoded, decode it
* Join your method path, the NULL `\0` character, and your post data into a string
* Generate an HMAC hash string, using your decoded secret as the secret, SHA-512 as the hash function, and your constructed string as the message
* Encode this HMAC string using base-64

Here is an example python implementation of all the above:

	:::python
	import hmac, base64, hashlib, urllib, urllib2, time, gzip, json, io
	base = 'https://data.mtgox.com/api/2/'
	key = ""
	sec = ""

	def sign(path, data):
		mac = hmac.new(base64.b64decode(sec), path+chr(0)+data, hashlib.sha512)
		return base64.b64encode(str(mac.digest()))

	def req(path, inp={}, get=False):
		try:
			headers = {
				'User-Agent': "Trade-Bot",
				'Accept-Encoding': 'GZIP',
			}
			if get:
				get_data = urllib.urlencode(inp)
				url = base + path + "?" + get_data
				request = urllib2.Request(url, headers=headers)
				response = urllib2.urlopen(request)
			else:
				inp[u'tonce'] = str(int(time.time()*1e6))
				post_data = urllib.urlencode(inp)
				headers.update({
					'Rest-Key': key,
					'Rest-Sign': sign(path, post_data),
					'Content-Type': 'application/x-www-form-urlencoded',
				})
				request = urllib2.Request(base + path, post_data, headers)
				response = urllib2.urlopen(request, post_data)
		except urllib2.HTTPError as e:
			response = e.fp
		enc = response.info().get('Content-Encoding')
		if isinstance(enc, str) and enc.lower() == 'gzip':
			buff = io.BytesIO(response.read())
			response = gzip.GzipFile(fileobj=buff)
		output = json.load(response)
		return output

	print req('BTCUSD/money/ticker_fast', {}, True)
	print req('BTCUSD/money/info')

The example in the code above uses a method that requires GET. Make sure to use POST for methods returning private data or performing private actions, and GET for public methods.

#### Tonces

A new addition to the authentication mechanism is the `tonce`. This is an alternative to `nonce` values. Whilst a nonce can be any number, as long as it is always increased between requests, the tonce is more standardised, and may be more reliable for those who have had trouble accessing the API. If you use tonces, each request must have a unique tonce value. The tonce is the unix microtime (number of microseconds since 00:00:00 UTC on 1 January 1970), and must be accurate to within +/- 10 seconds. The key difference is that, as long as it is unique *and* accurate to within +/- 10 seconds, it doesn't have to be ever increasing.

As you may expect, the parameter name is `tonce`, here is an example implementation in python (assuming `params` is a dict of method parameters.)

```
:::python
import time, urllib, urllib2
tonce = str(int(time.time() * 1e6))
params['tonce'] = tonce
data = urllib.urlencode(params)
req = makereq('my key', 'my secret', 'BTCUSD/money/ticker', data)
urllib2.urlopen(req, data)
```

**N.B.** Nonce and tonce values are only needed for authenticated POST rquests, GET requests only need the relevant arguments.

#### HMAC Troubleshooting

The process for computing the Rest-Sign is the following:

* Decode API **secret** using **Base64**
* Perform the **HMAC** algorithm on your message using **SHA-512** for the encryption method and the decoded secret above
	* Message is `path + "\0" + post_data` where path is the bit that comes *after* `https://data.mtgox.com/api/2/`
* Encode the HMAC **result** using **Base64**
* This is the `Rest-Sign`

To test your Rest-Signs, use the following pseudocode:

```
function hmac_512(msg, sec) {
	sec = Base64Decode(sec);
	result = hmac(msg, sec, sha512);
	return Base64Encode(result);
}


secret = "7pgj8Dm6";
message = "Test\0Message";

result = hmac_512(message, secret);
if (result == "69H45OZkKcmR9LOszbajUUPGkGT8IqasGPAWqW/1stGC2Mex2qhIB6aDbuoy7eGfMsaZiU8Y0lO3mQxlsWNPrw==")
	print("Success!");
else
	printf("Error: %s", result);
```

If your Rest-Sign is being generated correctly, but you still can't get a successful result from the API, make sure you are remembering to post the data. Also ensure that your nonce is always greater than whatever you've sent before - if you suspect it isn't, generate a new API key/secret pair. If unsure, use unix microtime for your nonce (you can pad the millitime, or plain timestamp, with "000" or "000000" respectively to achieve this if necessary). 

##### Javascript

If you are using JavaScript, please note that CryptoJS is not able to produce the above results. I suggest using [jsSHA](http://caligatio.github.io/jsSHA/) instead. You can use it as follows:

```
:::javascript
// secret should be provided in the form given by MtGox (ie, Base64 encoded) as jsSHA will handle all the decoding internally
function hmac_512(message, secret) {
	var shaObj = new jsSHA(message, "TEXT");
	var hmac = shaObj.getHMAC(secret, "B64", "SHA-512", "B64");
	return hmac
}
alert(hmac_512("Test\0Message","7pgj8Dm6") == "69H45OZkKcmR9LOszbajUUPGkGT8IqasGPAWqW/1stGC2Mex2qhIB6aDbuoy7eGfMsaZiU8Y0lO3mQxlsWNPrw==" ? "Success!" : "Failure :/");
```


### Requests

Each request consists of a path, any arguments, and a nonce value. The different methods you can use are defined by their paths and the arguments they accept. The path is the part of the HTTP URL after the base.

**Base**: <https://data.mtgox.com/api/2/>     
**Path**: [BTCUSD/money/ticker](https://data.mtgox.com/api/2/BTCUSD/money/ticker)

The nonce value is important to prevent duplicated requests. In short, each request should be accompanied by a unique nonce value in the post data, and this value should be an integer and larger than any value sent previously. One way to accomplish this is by using millisecond or microsecond unix time, e.g. if your computer supports microsecond resolution:

	:::python
	import time
	def nonce():
		return str(int(time.time() * 1e6))

This should be added to your argument list, and then submitted using url form encoding, i.e.:

	nonce=123&type=bid&amount_int=765000000&price_int=6061000

Some requests don't need any post data or HMAC authentication, such as the BTCUSD/money/ticker example used, and can be easily obtained in a web browser, but in general, you should always authenticate with a valid API key, and make sure that the key has the necessary rights.

Note that the API will ignore any extraneous arguments.

### WebSockets

There is some information about WebSockets here: <https://bitbucket.org/nitrous/mtgox-api/src/master/socket>

## Methods

**Disclaimer:** Official documentation for the methods below, at the time of writing, is limited or even non-existent, and the following information has been pooled from multiple sources, found by trial and error, or extrapolated. No responsibility is taken by the author for any incorrect or incomplete information, nor for any unintended effects it may cause. _Use the information below at your own risk._

---

Each of the methods described below consists of a table of key/value pairs if there are any, which are the known acceptable arguments, a sample request, and some brief notes.

### Glossary

Term | Definition
---- | ----
auxiliary currency | The currency specified, other than `BTC`, e.g. the auxiliary currency for the pair `BTCUSD` would be `USD`.
integer value | A currency value given in terms of the minimum division, for example `$ 2.37` would be `237000`. See the currency section for more information. Note, that even though these values are _ostensibly_ meant to be integers, sometimes a float may be returned, for example for small order quotes, so you should round these values if necessary.
bid | A `bid` order typically refers to a buy order for BTC using the auxiliary currency.
ask | An `ask` order typically refers to a sell order of BTC for the auxiliary currency.

#### Code

`**Currency Object**` refers to a JSON block of the form:

```
:::json
{
	"currency": "BTC",
	"display": "17.96800010\u00a0BTC",
	"display_short": "17.97\u00a0BTC",
	"value": "17.96800010",
	"value_int": "1796800010"
}
```

#### Contents

* [money](#markdown-header-money)
	* [info](#markdown-header-moneyinfo)
	* [idkey](#markdown-header-moneyidkey)
	* [orders](#markdown-header-moneyorders)
	* order
		* [quote](#markdown-header-moneyorderquote)
		* [add](#markdown-header-moneyorderadd)
		* [cancel](#markdown-header-moneyordercancel)
		* [result](#markdown-header-moneyorderresult)
		* [lag](#markdown-header-moneyorderlag)
	* [currency](#markdown-header-moneycurrency)
	* [ticker](#markdown-header-moneyticker)
	* [ticker_fast](#markdown-header-moneyticker_fast)
	* [hotp_gen](#markdown-header-moneyhotp_gen)
	* [quote](#markdown-header-moneyquote)
	* [trades](#markdown-header-moneytrades)
		* [fetch](#markdown-header-moneytradesfetch)
		* [cancelled](#markdown-header-moneytradescancelled)
	* [cancelledtrades](#markdown-header-moneycancelledtrades)
	* [depth](#markdown-header-moneydepth)
		* [fetch](#markdown-header-moneydepthfetch)
		* [full](#markdown-header-moneydepthfull)
	* [fulldepth](#markdown-header-moneyfulldepth)
	* wallet
		* [history](#markdown-header-moneywallethistory)
	* bitcoin
		* [get_address](#markdown-header-moneybitcoinget_address)
* [security](#markdown-header-security_1)
	* hotp
		* [gen](#markdown-header-securityhotpgen)
* [stream](#markdown-header-stream)
	* [list_public](#markdown-header-streamlist_public)
* [others](#markdown-header-other-methods)

### Money

#### `money/info`

This method provides a lot of information about your account, the example given below is not actual sample data (as this would be personally identifying!), but is a skeleton of the data you can expect:

1 **Request:** `BTCUSD/money/info`   
**Response:**

	:::json
	{
		"data": {
			"Created": "yyyy-mm-dd hh:mm:ss",
			"Id": "abc123",
			"Index": "123456",
			"Language": "en_US",
			"Last_Login": "yyyy-mm-dd hh:mm:ss",
			"Link": "M78123456X",
			"Login": "username",
			"Monthly_Volume":                   **Currency Object**,
			"Trade_Fee": 0.6,
			"Rights": ['deposit', 'get_info', 'merchant', 'trade', 'withdraw'],
			"Wallets": {
				"BTC": {
					"Balance":                  **Currency Object**,
					"Daily_Withdraw_Limit":     **Currency Object**,
					"Max_Withdraw":             **Currency Object**,
					"Monthly_Withdraw_Limit": null,
					"Open_Orders":              **Currency Object**,
					"Operations": 1,
				},
				"USD": {
					"Balance":                  **Currency Object**,
					"Daily_Withdraw_Limit":     **Currency Object**,
					"Max_Withdraw":             **Currency Object**,
					"Monthly_Withdraw_Limit":   **Currency Object**,
					"Open_Orders":              **Currency Object**,
					"Operations": 0,
				},
				"JPY":{...}, "EUR":{...},
				// etc, depends what wallets you have
			},
		},
		"result": "success"
	}

* The `Open_Orders` values appear to be deprecated, as regardless of what orders you actuall have open, this currency object will always be 0, at leat in my tests; To get currently open orders, use [`money/orders`](#markdown-header-moneyorders) instead
* `Trade_Fee` is the current percentage fee taken by MtGox on your orders, as a decimal
* You do not need to include a currency for this method
* Link is your MtGox account ID - it contains Index and a checksum

[^ Back to contents](#markdown-header-contents)

#### `money/idkey`

Returns an `idKey` used to subscribe to a user's private updates in the websocket API[^1]. The key is valid for 24 hours. Without any arguments, it will return your personal `idKey`, there does not appear to be a way to obtain other user's keys, and this may be intentional in order to protect their privacy. 

1 **Request:** `BTCUSD/money/idkey`   
**Response:** `{"result":"success","data":"abc+\/123=.."}`

* You do not need to include a currency for this method
* Forward slashes are escaped by a single backslash

[^ Back to contents](#markdown-header-contents)

#### `money/orders`

Get information on your current orders

1 **Request:** `BTCUSD/money/orders`   
**Response:** `{"result":"success","data":[...]}`

* Each datum object has the following format:

```
:::json
{
	"oid": "abc123-def456-..",
	"currency": "USD",
	"item": "BTC",
	"type": "bid",
	"amount":           **Currency Object**,
	"effective_amount": **Currency Object**,
	"invalid_amount":   **Currency Object**,
	"price":            **Currency Object**,
	"status": "pending",
	"date": 1365517594,
	"priority": "1365517594817935",
	"actions": []
}
```

* `amount` is the total amount of the order
* `effective_amount` is the amount of the order that can be fullfilled. If the entirety of the order would be fullfiled, `effective_amount` and `amount` will be equal. See `invalid_amount` otherwise
* `invalid_amount` will only be returned if part of (or an entire ??) order would not be fullfiled (e.g. not enough funds currently in your account)
* `price` is the price per bitcoin in the relevant currency, note that for a market order, this will be 0
* Statuses may be: `pending`, `executing`, `post-pending`, `open`, `stop`, and `invalid`, according to v1
* It seems priority is a unix timestamp with microsecond precision
* You do not need to include a currency for this method
* It is unknown what purpose `actions` serves

[^ Back to contents](#markdown-header-contents)

#### `money/currency`

Get information for a given currency

1 **Request:** `BTCUSD/money/currency`   
**Response:**

```
:::json
{
	"result":"success",
	"data": {
		"currency":"USD",
		"name":"Dollar",
		"symbol":"$",
		"decimals":"5",
		"display_decimals":"2",
		"symbol_position":"before",
		"virtual":"N",
		"ticker_channel":"abc123-def456",
		"depth_channel":"abc123-def456"
	}
}
```

* There does not seem to be a way to get similar information for BTC using this API
* The most important parts of this data is available in the currency section above, however it may be useful to use this method if you think the information may change
* You may also need this data if you don't want to hard code the way you display currencies, or if you wish to track the currency using the websocket API[^1].

[^ Back to contents](#markdown-header-contents)

#### `money/ticker`

Get the most recent information for a currency pair

1 **Request:** `BTCUSD/money/ticker`   
**Response:**

```
:::json
{
	"result":"success",
	"data": {
		"high":       **Currency Object - USD**,
		"low":        **Currency Object - USD**,
		"avg":        **Currency Object - USD**,
		"vwap":       **Currency Object - USD**,
		"vol":        **Currency Object - BTC**,
		"last_local": **Currency Object - USD**,
		"last_orig":  **Currency Object - ???**,
		"last_all":   **Currency Object - USD**,
		"last":       **Currency Object - USD**,
		"buy":        **Currency Object - USD**,
		"sell":       **Currency Object - USD**,
		"now":        "1364689759572564"
	}
}
```

* `vwap` is the [volume-weighted average price](http://en.wikipedia.org/wiki/Volume-weighted_average_price)
* `last_local` is the last trade in your selected auxiliary currency
* `last_orig` is the last trade (any currency)
* `last_all` is that last trade converted to the auxiliary currency
* `last` is the same as `last_local`
* `now` is the unix timestamp, but with a resolution of 1 microsecond

[^ Back to contents](#markdown-header-contents)

#### `money/ticker_fast`

Get the most recent information for a currency pair. This method is similar to `money/ticker`, except it returns less information, and is supposedly lag-free.

1 **Request:** `BTCUSD/money/ticker_fast`   
**Response:**

```
:::json
{
	"result":"success",
	"data": {
		"last_local": **Currency Object - USD**,
		"last":       **Currency Object - USD**,
		"last_orig":  **Currency Object - EUR**,
		"last_all":   **Currency Object - USD**,
		"buy":        **Currency Object - USD**,
		"sell":       **Currency Object - USD**,
		"now":        "1366230242125772"
	}
}
```

* **In my tests, I found no significant difference between `ticker` and `ticker_fast`: ** *(T is `ticker`, F is `ticker_fast`, during a ~60s lag period, results are response times in seconds)*

		T: 0.886923 	 F: 0.642117
		T: 0.488146 	 F: 0.584797
		T: 0.696593 	 F: 0.713574
		T: 0.981266 	 F: 0.819817
		T: 0.556972 	 F: 0.750306
		T: 0.566301 	 F: 0.629578
		T: 0.818712 	 F: 0.695444
		T: 0.651754 	 F: 0.951485
		T: 0.474978 	 F: 0.769460
		T: 0.669930 	 F: 0.740889

* This is a new method designed to better cope with high server load.
* In comparison to `money/ticker`, this method has a lot less information - it doesn't give the `high`, `low`, `avg`, `vwap`, or `vol` properties.
* Additionally, it apparently has a 1 second cache, and there is no rate limit (see [Reddit Thread](http://www.reddit.com/r/Bitcoin/comments/1c9npl/mtgox_fast_ticker_last_only_no_rate_limit_1second)), although this doesn't mean you should abuse it.
* I have not yet been able to test the performance difference between `money/ticker` and `money/ticker_fast` during a high lag situation, however if the above is true, this method should remain accessible even if the rest of the API is down (as it should be cached by [CloudFlare](http://www.cloudflare.com)).
* The information presented by each property is the same as in `money/ticker_fast`:
	* `last_local` is the last trade in your selected auxiliary currency
	* `last_orig` is the last trade (any currency)
	* `last_all` is that last trade converted to the auxiliary currency
	* `last` is the same as `last_local`
	* `now` is the unix timestamp, but with a resolution of 1 microsecond

[^ Back to contents](#markdown-header-contents)

#### `money/hotp_gen`
Deprecated - Use [`security/hotp/gen`](#markdown-header-securityhotpgen)

[^ Back to contents](#markdown-header-contents)

#### `money/quote`
Deprecated - Use [`money/order/quote`](#markdown-header-moneyorderquote)

[^ Back to contents](#markdown-header-contents)

#### `money/trades`
Deprecated - Use [`money/trades/fetch`](#markdown-header-moneytradesfetch)

[^ Back to contents](#markdown-header-contents)

#### `money/cancelledtrades`
Deprecated - Use [`money/trades/cancelled`](#markdown-header-moneytradescancelled)

[^ Back to contents](#markdown-header-contents)

#### `money/depth`
Deprecated - Use [`money/depth/fetch`](#markdown-header-moneydepthfetch)

[^ Back to contents](#markdown-header-contents)

#### `money/fulldepth`
Deprecated - Use [`money/depth/full`](#markdown-header-moneydepthfull)

[^ Back to contents](#markdown-header-contents)

---

#### `money/order/quote`

Get an up-to-date quote for a bid or ask transaction

key | value
--- | ---
type | `bid` or `ask`
amount | amount of BTC to buy or sell, as an integer

1 **Request:** `BTCUSD/money/order/quote` ~ `type=bid&amount=100000000`   
**Response:** `{"result":"success","data":{"amount":9197999}}`

2 **Request:** `BTCUSD/money/order/quote` ~ `type=ask&amount=23769001`   
**Response:** `{"result":"success","data":{"amount":2179142.24937}}`

* This method differs from most, as the currency values are _always_ given as integers
* As noted in the glossary, the api isn't strict about giving integers, see the second example - the quote for selling `0.23769001 BTC` is given as `$ 21.7914224937`, more digits than are expected for a USD value, so you may need to round this data to the nearest integer
* Regardless of whether your order is `bid` or `ask`, your input should be the quantity of bitcoins and the response will be in the auxiliary currency 

[^ Back to contents](#markdown-header-contents)

#### `money/order/add`

Creates a currency trade order

key | value
--- | ---
type | `bid` or `ask`
amount_int | amount of BTC to buy or sell, as an integer
price_int | The price per bitcoin in the auxiliary currency, as an integer, optional if you wish to trade at the market price
_amount_ | Deprecated - Use `amount_int`
_price_ | Deprecated - Use `price_int`

1 **Request:** `BTCUSD/money/order/add` ~ `type=bid&amount_int=50000000&price_int=9250000`   
**Response:** `{"result":"success","data":"abc123-def45-.."}`

* The amount is always given in BTC
* Price is always in the auxiliary currency, and is per bitcoin
* If you omit the price, your trade will be treated as a market order, and completed at the current market price
* `data` returns the order id, which can then be manipulated using other methods

[^ Back to contents](#markdown-header-contents)

#### `money/order/cancel`

Cancel a trade order

key | value
--- | ---
oid | The `id` of the order you wish to cancel

1 **Request:** `BTCUSD/money/order/cancel` ~ `oid=abc123-def45-...`   
**Response:** `{"result":"success","data":{"oid":"abc123-def45-...","qid":"ghi678-jkl90-..."}}`

* It appear that you do not need to specify what type of order you are cancelling, just the `id`
* It goes without saying that you cannot cancel an order that has already completed
* You *may*, however, be able to cancel a partially completed order, I am not sure though
* It is unknown what the purpose of `qid` is

[^ Back to contents](#markdown-header-contents)

#### `money/order/result`

Get information on a trade order

key | value
--- | ---
type | `bid` or `ask`
order | The `id` of the order

1 **Request:** `BTCUSD/money/order/result` ~ `type=bid&order=123456789-0abc-def0-1234-567890abcdef`    
**Response:**  
```
:::json
{"result":"success","data":{
	"avg_cost":       **Currency Object - aux**,
	"order_id": "123456789-0abc-def0-1234-567890abcdef",
	"total_amount":   **Currency Object - BTC**,
	"total_spent":    **Currency Object - aux**,
	"trades":[
		{
			"amount": **Currency Object - BTC**,
			"currency": "aux",
			"date": "2013-05-31 18:20:40",
			"item": "BTC",
			"price":  **Currency Object - aux**,
			"primary": "Y",
			"properties": "market",
			"trade_id": "1370024440269589",
			"type": "ask"
		},
		{
			"amount": **Currency Object - BTC**,
			"currency": "aux",
			"date": "2013-05-31 18:20:40",
			"item": "BTC",
			"price":  **Currency Object - aux**,
			"primary": "Y",
			"properties": "market",
			"trade_id": "1370024440644975",
			"type": "ask"
		}
	]
}}
```

* This method only works on completed orders. For open orders, see [`money/orders`](#markdown-header-moneyorders). Cancelled orders are not returned by this method unless they have been partially filled before cancelling, in which case they do.
* The example order above was a market order, and was completed in two steps
* `total_amount` is the amount bought/sold in BTC
* `total_spent` is the amount spent **or** received in the auxiliary currency (the key is a bit misrepresentative, for example in this case it actually represents the amount of USD that I *received*)
* `order_id` should be the same as the order you specified
* The `date` is given in ISO format

*Thanks to [mikhail_tomsk](https://bitcointalk.org/index.php?topic=220787.0)*

[^ Back to contents](#markdown-header-contents)

#### `money/order/lag`

Returns the lag time for executing orders in microseconds

1 **Request:** `BTCUSD/money/order/lag`   
**Response:** `{"result":"success","data":{"lag":2319790,"lag_secs":2.31979,"lag_text":"2.31979 seconds"}}`

* If the MtGox trade queue is empty, the lag time will be 0

[^ Back to contents](#markdown-header-contents)

---

#### `money/trades/fetch`

This method gets up to 1000 trades from the date specified (or the most recent, if none)

key | value
--- | ---
since | The tid *after which* to retrieve trades. For most trades, this is the unix microstamp (see below). This is optional - if omitted, only the most recent 1000 trades will be returned.

*Time now is 1364767250*

**IMPORTANT NOTE: This method is unavailable by POST, use the following GET request**

1 **Request:** `BTCUSD/money/trades/fetch?since=1364767190000000`   
**Response:** 

```
:::json
{"result":"success","data":[
	{"date":1364767201,"price":"92.65","amount":"0.47909825","price_int":"9265000","amount_int":"47909825","tid":"1364767201381791","price_currency":"USD","item":"BTC","trade_type":"bid","primary":"Y","properties":"limit"},
	{"date":1364767207,"price":"92.65","amount":"0.01024284","price_int":"9265000","amount_int":"1024284","tid":"1364767207406066","price_currency":"USD","item":"BTC","trade_type":"bid","primary":"Y","properties":"limit"},
	{"date":1364767232,"price":"92.65","amount":"1.25637797","price_int":"9265000","amount_int":"125637797","tid":"1364767232522802","price_currency":"USD","item":"BTC","trade_type":"bid","primary":"Y","properties":"limit"},
	{"date":1364767236,"price":"92.65","amount":"0.01","price_int":"9265000","amount_int":"1000000","tid":"1364767236979043","price_currency":"USD","item":"BTC","trade_type":"bid","primary":"Y","properties":"limit"},
	{"date":1364767245,"price":"92.65","amount":"0.01","price_int":"9265000","amount_int":"1000000","tid":"1364767245879648","price_currency":"USD","item":"BTC","trade_type":"bid","primary":"Y","properties":"limit"}
]}
```

* In the above example, as the since date was 60s ago, only 60s of trades were returned
* The first 200,000+ trades used sequential integers for their tid values, up to 218868. After this, unix microstamps are used instead, starting with 1309108565842636.
* If you request data since, for example, 218500, you will only get data up to 218868, and to retrieve further data you will need to manually jump to the next date. There are other such jumps for other currencies.
* This behaviour was introduced to discourage the downloading of all trade data via this method. Therefore, please don't try to build a dump from scratch. It is probably fine to maintain an already up-to-date dump though.
* If you need *all* data, either find a pre-prepared dump or use Google's BigQuery service, which MtGox is using for a public database dump, more info here - <https://bitcointalk.org/index.php?topic=218980.0>
* The `primary` field specifies whether the trade was originally ordered in the given currency - since trades may appear on multiple tickers, you may need to ignore trades with `primary: 'N'` values
* As noted above, since gives the tid *after which* to obtain trades, so for example, passing `since=10` will return trade data starting with the trade whose `tid` is `11`.

[^ Back to contents](#markdown-header-contents)

#### `money/trades/cancelled`

Not implemented

This method appears to be completely useless, currently. Despite cancelling several of my own trades, none has ever appeared in the results of this method.

1 **Request:** `BTCUSD/money/trades/cancelled`   
**Response:** `{"result":"success","data":[]}`

* There are no known arguments
* The currency does not appear to be necessary, the same result is obtained for `money/trades/cancelled` as well
* It is unclear what the results would look like when this is eventually implemented, however it is likely that they will share a similar format with [`money/trades/fetch`](#markdown-header-moneytradesfetch)

[^ Back to contents](#markdown-header-contents)

---

#### `money/depth/fetch`

Gets recent depth information

1 **Request:** `BTCUSD/money/depth/fetch`   
**Response:**

```
:::json
{"result":"success","data":{
	"asks":[...],
	"bids":[...],
	"filter_min_price":{"value":"83.48403","value_int":"8348403","display":"$83.48403","display_short":"$83.48","currency":"USD"},
	"filter_max_price":{"value":"102.03603","value_int":"10203603","display":"$102.03603","display_short":"$102.04","currency":"USD"}
}}
```

* The above data ranges from 1359589420235069 (Wed, 30 Jan 2013 23:43:40 GMT) to 1364770856550122 (Sun, 31 Mar 2013 23:00:57 GMT)
* Even though this data is about 6 times smaller than [`money/depth/full`](#markdown-header-moneydepthfull), it is still very big, so it is possible that excessive requests will also result in banning
* There are 852 bids and 839 asks in the above example
* Refer to [`money/depth/full`](#markdown-header-moneydepthfull) for detail on the ask and bid formats
* There is probably an argument that can be used to limit the amount of data returned, however it is currently unknown

[^ Back to contents](#markdown-header-contents)

#### `money/depth/full`

Gets a complete copy of MtGox's full depth data

1 **Request:** `BTCUSD/money/depth/full`   
**Response:** 

```
:::json
{"result":"success","data":{
	"asks":[
		{"price":93.06545,"amount":0.07284541,"price_int":"9306545","amount_int":"7284541","stamp":"1364769641591046"},
		{"price":93.06546,"amount":0.81618662,"price_int":"9306546","amount_int":"81618662","stamp":"1364769116973406"},
		{"price":93.4,"amount":0.02,"price_int":"9340000","amount_int":"2000000","stamp":"1364769080120861"},
		...
	],
	"bids":[
		{"price":1.0e-5,"amount":30694957.01,"price_int":"1","amount_int":"3069495700999999","stamp":"1364764907440546"},
		{"price":2.0e-5,"amount":10000.02,"price_int":"2","amount_int":"1000002000000","stamp":"1364418775588024"},
		{"price":3.0e-5,"amount":41515.8015,"price_int":"3","amount_int":"4151580150000","stamp":"1364225564960621"},
		...
	]
}}
```

* This file can be at least as big as 1MB, and so there is a rate limit of up to 5 requests per hour, if you exceed this you risk being banned
* The example above contained 7671 bids and 1929 asks
* The date range was from 1309117574539900 (Sun, 26 Jun 2011 19:46:15 GMT) to 1364769659729678 (Sun, 31 Mar 2013 22:41:00 GMT)

[^ Back to contents](#markdown-header-contents)

---

#### `money/wallet/history`

Gets the transaction history of a specified currency wallet.

*Lots of the information in this section was found by [Baracuda](https://bitcointalk.org/index.php?action=profile;u=91268), thanks :)*

key | value
--- | ---
currency | The currency of the wallet whose history you are requesting (required)
page | The page of history data to retrieve (optional)

1 **Request:** `money/wallet/history` ~ `currency=BTC`  
**Response:**

```
:::json
{
	"result": "success",
	"data": {
		"records": "17",
		"result": [
			{
				"Index": "17",
				"Date": 1366243097,
				"Type": "out",
				"Value":   **Currency Object**,
				"Balance": **Currency Object**,
				"Info": "BTC sold: [tid:1366243097811202] 0.25000000 BTC at $126.50000",
				"Link":[
					"123456789-0abc-def0-1234-567890abcdef",
					"Money_Trade",
					"1366243097811202"]
			},
			{
				"Index": "16",
				"Date": 1366218001,
				"Type": "deposit",
				"Value":   **Currency Object**,
				"Balance": **Currency Object**,
				"Info": "1BitcoinAddress789fhjka890jkl",
				"Link":[
					"123456789-0abc-def0-1234-567890abcdef",
					"Money_Bitcoin_Block_Tx_Out",
					"1BitcoinTransaction780gfsd8970fg:9"]
			},
			{...},
		],
		"current_page": 1,
		"max_page": 1,
		"max_results": 50
	}
}
```

* For a bitcoin transaction, `link` appears to be an array, of which:
	* [0] = The transaction UUID
	* [1] = Category of transaction
	* [2] = A specific identifier
	* See the known transaction types table below for a known list of these transaction types
* The results are returned in order of most recent
* Results are paged, with the maximum results per request set at 50, and the number of pages given by `max_page`
* You can access other pages using the `page` parementer
* `Value` is the amount transferred
* `Balance` is the balance immediately following this transaction
* `Type` is a unique symbol describing the transaction
* Trades appear to come in groups of three:
	* A bid order will result in: `spent`, `in`, `fee`
	* An ask order will result in: `out`, `earned`, `fee`
	* The TID (Link[2]) is the same for all three of these, but the UUID differs
	* `in` and `out` relate to your BTC wallet
	* `spent` and `earned` relate to your auxiliary wallet
	* `fee` comes from either wallet, depending on whether you configured your fees to be built into the order or taken from the cash balance (in your MtGox settings). The following table tells you whether the fee is taken from your BTC wallet or the auxiliary trade currency wallet:

`fee` | built-in | balance
--- | --- | ---
**bid** | *btc* | *aux*
**ask** | *aux* | *btc*

*Remember, **bid** refers to buying BTC using the auxiliary currency (e.g. USD), and **ask** refers to selling BTC for the auxiliary currency*


**Known transaction types:**

`Type` | Description
--- | ---
`withdraw` | Money being withdrawn (eg to bank account)
`deposit` | Incoming deposit of currency
`in` | BTC gained after a bid order
`spent` | Auxiliary currency removed after a bid order
`out` | BTC removed after an ask order
`earned` | Auxiliary currency gained after an ask order
`fee` | Fee deducted from balance, e.g. during an order. The currency from which the fee is taken depends on the type of order and whether you specified for the fee to be taken from your order or added on top.

**Known transaction links:**

Currency | [1] | [2] | `Info`
--- | --- | --- | ---
BTC | `Money_Bitcoin_Block_Tx_Out` | Bitcoin transaction ID, followed by a colon, followed by the index of your specific subtransaction | The bitcoin sending address
BTC | `Money_Trade` | The MtGox public TID, ie the unix microtimestamp when the transaction occurred | A summary in English



[^ Back to contents](#markdown-header-contents)

#### `money/bitcoin/get_address`

Returns a unique bitcoin deposit address for a given MtGox account (new each time).

key | value
--- | ---
account | Your MtGox account number, you can get this from money/data in the Link field. You can also put a message before/after, as ``My message M12345678X (this was a test)``

1 **Request:** `BTCUSD/money/bitcoin/get_address?address=M12345678X`   
**Response:** {"result":"success","data":"1bitcoin2address4789051578025524"}

2 **Request:** `BTCUSD/money/bitcoin/get_address?address=M12345678X&raw`   
**Response:** 1my2crypto3address4178950fjkl24578902578

[^ Back to contents](#markdown-header-contents)

---

### Security

**This category does not require the use of a currency pair**

#### `security/hotp/gen`

Generates a new HOTP key, what this is used for is unclear, though it may have some use for application developers

1 **Request:** `security/hotp/gen`   
**Response:**

```
:::json
{
	"result":"success",
	"data": {
		"serial":"123",
		"name":"OTP#123",
		"key":"abc456",
		"key_base32":"ABC789"
	}
}
```

[^ Back to contents](#markdown-header-contents)

### Stream

#### `stream/list_public`

Lists the public depth, ticker and trade channels supported by the streaming API. Note that many more currencies seem to be available with this, including Litecoins and Namecoins, although these are **not** available for trading over MtGox.

1 **Request:** `stream/list_public`  
**Response:**

```
:::json
{"result":"success","data":{
	"ticker.LTCGBP":"0102a446-e4d4-4082-8e83-cc02822f9172",
	"ticker.LTCCNY":"0290378c-e3d7-4836-8cb1-2bfae20cc492",
	"ticker.NMCAUD":"08c65460-cbd9-492e-8473-8507dfa66ae6",
	"ticker.BTCEUR":"0bb6da8b-f6c6-4ecf-8f0d-a544ad948c15",
	"ticker.BTCCAD":"10720792-084d-45ba-92e3-cf44d9477775",
	"ticker.LTCNOK":"13616ae8-9268-4a43-bdf7-6b8d1ac814a2",
	"ticker.LTCUSD":"1366a9f3-92eb-4c6c-9ccc-492a959eca94",
	"ticker.BTCBTC":"13edff67-cfa0-4d99-aa76-52bd15d6a058",
	"ticker.LTCCAD":"18b55737-3f5c-4583-af63-6eb3951ead72",
	"ticker.NMCCNY":"249fdefd-c6eb-4802-9f54-064bc83908aa",
	"ticker.BTCCHF":"2644c164-3db7-4475-8b45-c7042efe3413",
	"ticker.BTCCZK":"2a968b7f-6638-40ba-95e7-7284b3196d52",
	"ticker.BTCSGD":"2cb73ed1-07f4-45e0-8918-bcbfda658912",
	"ticker.NMCJPY":"314e2b7a-a9fa-4249-bc46-b7f662ecbc3a",
	"ticker.BTCNMC":"36189b8c-cffa-40d2-b205-fb71420387ae",
	"ticker.BTCLTC":"48b6886f-49c0-4614-b647-ba5369b449a9",
	"ticker.LTCEUR":"491bc9bb-7cd8-4719-a9e8-16dad802ffac",
	"ticker.BTCINR":"55e5feb8-fea5-416b-88fa-40211541deca",
	"ticker.LTCJPY":"5ad8e40f-6df3-489f-9cf1-af28426a50cf",
	"ticker.BTCNZD":"5ddd27ca-2466-4d1a-8961-615dedb68bf1",
	"ticker.BTCSEK":"6caf1244-655b-460f-beaf-5c56d1f4bea7",
	"ticker.BTCNOK":"7532e866-3a03-4514-a4b1-6f86e3a8dc11",
	"ticker.BTCGBP":"7b842b7d-d1f9-46fa-a49c-c12f1ad5a533",
	"ticker.NMCUSD":"9aaefd15-d101-49f3-a2fd-6b63b85b6bed",
	"ticker.LTCAUD":"a046600a-a06c-4ebf-9ffb-bdc8157227e8",
	"ticker.BTCJPY":"a39ae532-6a3c-4835-af8c-dda54cb4874e",
	"ticker.LTCDKK":"b10a706e-e8c7-4ea8-9148-669f86930b36",
	"ticker.BTCPLN":"b4a02cb3-2e2d-4a88-aeea-3c66cb604d01",
	"ticker.BTCRUB":"bd04f720-3c70-4dce-ae71-2422ab862c65",
	"ticker.NMCGBP":"bf5126ba-5187-456f-8ae6-963678d0607f",
	"ticker.BTCKRW":"bf85048d-4db9-4dbe-9ca3-5b83a1a4186e",
	"ticker.BTCCNY":"c251ec35-56f9-40ab-a4f6-13325c349de4",
	"ticker.BTCHKD":"d3ae78dd-01dd-4074-88a7-b8aa03cd28dd",
	"ticker.BTCTHB":"d58e3b69-9560-4b9e-8c58-b5c0f3fda5e1",
	"ticker.BTCUSD":"d5f06780-30a8-4a48-a2f8-7ed181b4a13f",
	"ticker.NMCEUR":"d8512d04-f262-4a14-82f2-8e5c96c15e68",
	"ticker.NMCCAD":"dc28033e-7506-484c-905d-1c811a613323",
	"ticker.BTCDKK":"e5ce0604-574a-4059-9493-80af46c776b3",
	"ticker.BTCAUD":"eb6aaa11-99d0-4f64-9e8c-1140872a423d",
	
	"depth.BTCHKD":"049f65dc-3af3-4ffd-85a5-aac102b2a579",
	"depth.BTCEUR":"057bdc6b-9f9c-44e4-bc1a-363e4443ce87",
	"depth.BTCKRW":"0c84bda7-e613-4b19-ae2a-6d26412c9f70",
	"depth.BTCCNY":"0d1ecad8-e20f-459e-8bed-0bdcf927820f",
	"depth.BTCCHF":"113fec5f-294d-4929-86eb-8ca4c3fd1bed",
	"depth.BTCUSD":"24e67e0d-1cad-4cc0-9e7a-f8523ef460fe",
	"depth.BTCAUD":"296ee352-dd5d-46f3-9bea-5e39dede2005",
	"depth.BTCINR":"414fdb18-8f70-471c-a9df-b3c2740727ea",
	"depth.BTCSGD":"41e5c243-3d44-4fad-b690-f39e1dbb86a8",
	"depth.BTCCAD":"5b234cc3-a7c1-47ce-854f-27aee4cdbda5",
	"depth.BTCGBP":"60c3af1b-5d40-4d0e-b9fc-ccab433d2e9c",
	"depth.BTCNOK":"66da7fb4-6b0c-4a10-9cb7-e2944e046eb5",
	"depth.BTCTHB":"67879668-532f-41f9-8eb0-55e7593a5ab8",
	"depth.BTCSEK":"8f1fefaa-7c55-4420-ada0-4de15c1c38f3",
	"depth.BTCDKK":"9219abb0-b50c-4007-b4d2-51d1711ab19c",
	"depth.BTCJPY":"94483e07-d797-4dd4-bc72-dc98f1fd39e3",
	"depth.BTCCZK":"a7a970cf-4f6c-4d85-a74e-ac0979049b87",
	"depth.BTCNZD":"cedf8730-bce6-4278-b6fe-9bee42930e95",
	"depth.BTCRUB":"d6412ca0-b686-464c-891a-d1ba3943f3c6",
	"depth.BTCPLN":"e4ff055a-f8bf-407e-af76-676cad319a21",
	
	"trade.BTC":"dbf1dee9-4f2e-4a08-8cb7-748919a71b21",
	"trade.lag":"85174711-be64-4de1-b783-0628995d7914"
}}
```

* I have sorted the above lines into categories, but they do come randomly assorted.
* Other streams that are available are your user updates, retrievable with [`money/idkey`](#markdown-header-moneyidkey), and accessible using `{"op":"mtgox.subscribe","key":"--- your id-key ---"}`

[^ Back to contents](#markdown-header-contents)

---

### Other methods

The following methods have yet to be documented. They mainly revolve around bank management, merchant transactions, and direct bitcoin transfers. Any known arguments are listed below each relevant method.

* **Manage registered bank accounts**
	* `money/bank/register`
	* `money/bank/list`:
		* _no arguments_
* **Japanese banks**
	* `money/japan/lookup_bank`
	* `money/japan/lookup_branch`
* **Deal directly with bitcoins**
	* `money/bitcoin/addpriv`:
		* `keytype`
		* `key`
		* `description`
		* `raw`
		* `sha256`
		* `auto`
	* `money/bitcoin/address`:
		* `description`
		* `ipn`
	* `money/bitcoin/wallet_add`:
		* `wallet`
	* `money/bitcoin/send_simple`:
		* `address`
		* `amonut_int`
		* `fee_int`
		* `no_instant`
		* `green`
		* **Note:** You are not able to perform this method if you have enabled YubiKey/Two-Factor authentication for withdrawals. 2F auth does not seem to affect other methods, such as getting info, so this may or may not be a bug.
	* `money/bitcoin/null`
	* `money/bitcoin/block_list_tx`
	* `money/bitcoin/tx_details`
	* `money/bitcoin/addr_details`
	* `money/bitcoin/vanity_lookup`
* **Bitinstant transactions**
	* `money/bitinstant/quote`
	* `money/bitinstant/fee`
* **Merchant transactions**
	* `money/merchant/order/create`
	* `money/merchant/order/pay`
	* `money/merchant/order/details`
	* `money/merchant/order/payment`
	* `money/merchant/pos/order/create`
	* `money/merchant/pos/order/close`
	* `money/merchant/pos/order/get`
	* `money/merchant/pos/order/add_product`
	* `money/merchant/pos/order/edit_product`
	* `money/merchant/product/add`
	* `money/merchant/product/del`
	* `money/merchant/product/get`
	* `money/merchant/product/edit`
* **Misc**
	* `money/code/list`
	* `money/code/redeem`
	* `money/swift/details`
	* `money/ticket/create`:
		* `currency`
		* `amount_int`
		* `partner`
	* `money/token/process`

[^ Back to contents](#markdown-header-contents)

---

Tips and donations: `1VvKzcFibZdMyQdbXpSYcyoYvBsLLodDa`

[^1]: https://en.bitcoin.it/wiki/MtGox/API/Streaming
